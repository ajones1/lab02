import java.util.Scanner;


public class SimpleParenMatch 
{

    public static void main(String[] args) 
    {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.next();
        if (parensMatch(s)) 
        {
            System.out.println("The parentheses are balanced.");
        } 
        else 
        {
            System.out.println("Imbalanced!");
        }
    }

    public static boolean parensMatch(String s) 
    {
        boolean matches = true;
        int i = 0;
        int parenCount = 0;
        while (matches && i < s.length()) 
        {
            char c = s.charAt(i);
            if (c == '(') 
            {
                parenCount++;
            } 
            else if (c == ')') 
            {
                if (parenCount > 0) 
                {
                    parenCount--;
                } 
                else 
                {
                    matches = false;
                }
            }
            i++;
        }
        if (parenCount != 0) 
        {
            matches = false;
        }
        return matches;
    }

}
