/**
 * Stacks of strings using lists (using resizable arrays).
 * 
 * @author <YOUR NAME(s) HERE>
 */
public class Stack implements StackInterface 
{

    private StringListInterface list;
    
    public Stack()
    {
        list = new StringList();
    }
    
    public boolean isEmpty()
    {
        return list.isEmpty();
    }
    
    public void push(String s)
    {
        list.add(s);
    }
    
    public String pop()
    {
        String s = list.get(list.size() -1);
        list.remove(list.size()-1);
        return s;
    }

}
