import java.util.Scanner;


/**
 * Testing stacks via reversing sequence of strings read from standard input.
 *
 * @author Michael Siff
 */
public class StackTester {
    public static void main(String[] args) {
        String s;
        Scanner scanner = new Scanner(System.in);
        StackInterface stack = new Stack();

        do {
            s = scanner.nextLine();
            if (s.length() > 1) {
                stack.push(s);
            }
        } while (s.length() > 1);

        System.out.println("\nreversed output:\n");
        
        while (!stack.isEmpty()) {
            System.out.println(stack.pop());
        }
    }
}
